package com.example.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Subtraction extends AppCompatActivity {
    private Button buttonChange;
    private ImageView imageChange;
    private boolean imageFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtraction);
        addListenerOnButton();
    }

    @SuppressLint("SetTextI18n")
    public void sub(View v) {
        EditText el1 = findViewById(R.id.Num1Sub);
        EditText el2 = findViewById(R.id.Num2Sub);
        TextView resText = findViewById(R.id.ResultSub);

        int number1 = Integer.parseInt(el1.getText().toString());
        int number2 = Integer.parseInt(el2.getText().toString());
        resText.setText(Integer.toString(number1 - number2));
    }

    public void addListenerOnButton() {
        buttonChange = findViewById(R.id.buttonChange);
        imageChange = findViewById(R.id.imageChange);
        buttonChange.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (imageFlag) {
                            imageChange.setImageResource(R.drawable.false_image);
                            imageFlag = false;
                        } else {
                            imageChange.setImageResource(R.drawable.true_image);
                            imageFlag = true;
                        }
                    }
                }
        );
    }
}
