package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button buttonCloseApp;
    private Button buttonSubtraction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    @SuppressLint("SetTextI18n")
    public void sum(View v) {
        EditText el1 = findViewById(R.id.Num1Add);
        EditText el2 = findViewById(R.id.Num2Add);
        TextView resText = findViewById(R.id.ResultAdd);

        int number1 = Integer.parseInt(el1.getText().toString());
        int number2 = Integer.parseInt(el2.getText().toString());
        resText.setText(Integer.toString(number1 + number2));
    }

    @SuppressLint("CutPasteId")
    public void addListenerOnButton() {
        buttonCloseApp = findViewById(R.id.buttonCloseApp);
        buttonSubtraction = findViewById(R.id.buttonSubtraction);
        buttonCloseApp.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder a_buider = new AlertDialog.Builder(MainActivity.this);
                        a_buider.setMessage("Close application?")
                                .setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = a_buider.create();
                        alert.setTitle("Сlosing the application");
                        alert.show();
                    }
                }
        );

        buttonSubtraction.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent("com.example.myapplication.Subtraction");
                        startActivity(intent);
                    }
                }
        );
    }
}
